# Algorithms

## Introduction
In this reposority you will find my working solutions for specific algorithms aswell as puzzles of the platform https://www.codingame.com/ and https://www.algoexpert.io/.

> **Info:** It is not my intention to write the solutions as short as possible! I just want to write my code understandable and clean even when it makes the actual code longer (or at least sometimes). The solutions are attempts on getting into different algorithms.

## Disclaimer
I created all the solutions found here for myself. There may be similarities with solutions of others. These similarities are then random and not intended.
