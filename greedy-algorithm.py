"""
Introduction:
    A greedy algorithm is any algorithm that makes the locally optimal choice at each stage. 
    In many problems, a greedy strategy does not usually produce an optimal solution, 
    but nonetheless a greedy algorithm may yield locally optimal solutions 
    that approximate a globally optimal solution in a reasonable amount of time.

Goal:
    In a CardFight® game, players are fighting each others by assembling a deck of cards 
    and use strategy and tactics to defeat their opponents. Each card has a health point recorded as an integer. 
    Cards with too low health points are quite useless in the battle arena.

    However, low health point cards still have their value. 
    CardFight® company is offering a conversion service, 
    which is a machine allowing players to insert two cards, 
    and get output of one new card with a health point that equals to the sum of the two original cards.

    It seems there is no upper limit to the health points in cards produced by the machine. 
    Hurray! By making good use of this conversion machine, 
    you can trade-in your cards to get new cards with as high health points as you wish.

    You know the business rule - nothing is free. 
    Every time when you obtain a new card from the machine, 
    you have to pay a service fee proportional to the health point on the new card. 
    For example, if you insert an old 1-point card and another old 2-point card, 
    you will obtain a new card of 3 points. 
    CardFight® will debit you 3 dollars from your account.

    Find the best strategy to finish the trade-in with the lowest cost.

This file contains:
    greedy_trade_in()
"""

cards = [1, 2, 3, 5, 3, 7, 8, 9, 11, 15, 12, 13, 27, 38, 12, 15, 39, 40, 2, 7, 6]
cards = sorted(map(int, cards))
cost = 0

def greedy_trade_in(cards):
    while len(cards) > 1:
        newCard = cards[0] + cards[1]
        cost += newCard
        # add new card and reduce integer list --> e.g. [1 + 2 + 3] = [3 + 3]
        cards = sorted([newCard] + cards[2:])

greedy_trade_in(cards)
print(cost)